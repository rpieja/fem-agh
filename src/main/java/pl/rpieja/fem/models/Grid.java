package pl.rpieja.fem.models;

import java.util.ArrayList;
import java.util.List;

public class Grid {
    private List<Node> ND = null;
    private List<Element> EL = null;
    private Props props;
    private int nh, ne;

    public Grid(Props globalData) {
        nh = globalData.getNh();
        ne = globalData.getNe();
        this.props = globalData;

        ND = new ArrayList<>(nh);
        EL = new ArrayList<>(ne);
    }

    public void setND(List<Node> ND) {
        this.ND = ND;
    }

    public void setEL(List<Element> EL) {
        this.EL = EL;
    }

    public List getND() {
        return ND;
    }

    public List getEL() {
        return EL;
    }

    public void generateGrid(){
        //first generate nodes list
        {
            double dx = props.getDx();
            double dy = props.getDy();

            for (int i = 0; i < props.getnB(); ++i)
                for (int j = 0; j < props.getnH(); ++j)
                    ND.add(new Node(i * dx, j * dy, i * props.getnB() + j, false));


        }
        //generate element list
        {
            for (int i = 0; i < props.getnB() - 1; ++i){
                for (int j = 0; j < props.getnH() - 1; ++j){
                    int [] tab = new int[4];
                    tab[0] = j + i*(props.getnH());
                    tab[3] = tab[0] + 1;

                    tab[1] = j + (i+1)* props.getnH();
                    tab[2] = tab[1] + 1;


                    EL.add(new Element().withArray(tab));
                }
            }
        }

    }

}
