package pl.rpieja.fem.models;

import java.io.FileInputStream;
import java.util.Properties;

public class Props {
    private double H;   //H is the height of field
    private double B;   //B is the width of field
    private double dy;
    private double dx;
    private Integer nH;     //number of nodes on height
    private Integer nB;     //number of nodes on width
    private Integer nh;     //number of nodes
    private Integer ne;     //number of elements

    public Props() {
        Properties properties = new Properties();

        try {
            FileInputStream in = new FileInputStream("app.properties");
            properties.load(in);
            in.close();

            nH = Integer.valueOf(properties.getProperty("nH"));
            nB = Integer.valueOf(properties.getProperty("nB"));
//            nh = Integer.valueOf(properties.getProperty("nh"));
//            ne = Integer.valueOf(properties.getProperty("ne"));
            H = Double.valueOf(properties.getProperty("H"));
            B = Double.valueOf(properties.getProperty("B"));
//            dy = Double.valueOf(properties.getProperty("dy"));
//            dx = Double.valueOf(properties.getProperty("dx"));

            nh=nH*nB;
            ne=(nB-1)*(nH-1);
            dx=B/(nB-1);
            dy=H/(nH-1);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double getH() {
        return H;
    }

    public double getB() {
        return B;
    }

    public double getDy() {
        return dy;
    }

    public double getDx() {
        return dx;
    }

    public int getnH() {
        return nH;
    }

    public int getnB() {
        return nB;
    }

    public int getNh() {
        return nh;
    }

    public int getNe() {
        return ne;
    }
}