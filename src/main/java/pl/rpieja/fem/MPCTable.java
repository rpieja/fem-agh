package pl.rpieja.fem;

public class MPCTable {

    private double [][] mpc2 = null;
    private double [][] mpc3 = null;

    public MPCTable() {

        mpc2 = new double[2][2];
        mpc2[0][0] = -0.577;
        mpc2[0][1] = 1.;
        mpc2[1][0] = 0.577;
        mpc2[1][1] = 1.;


        mpc3 = new double[3][2];
        mpc3[0][0] = -0.7745;
        mpc3[0][1] = 5/(double)9;
        mpc3[1][0] = 0.;
        mpc3[1][1] = 8/(double)9;
        mpc3[2][0] = 0.7745;
        mpc3[2][1] = 5/(double)9;

    }

    public double[][] getMpc2() {
        return mpc2;
    }

    public double[][] getMpc3() {
        return mpc3;
    }
}
