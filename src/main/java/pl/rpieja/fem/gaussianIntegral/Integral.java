package pl.rpieja.fem.gaussianIntegral;

import pl.rpieja.fem.MPCTable;
import pl.rpieja.fem.models.Grid;
import pl.rpieja.fem.models.Props;

import java.util.Scanner;

public class Integral {
    public static Double f(Double x, Double y){
        return (5*Math.pow(x, 2)*Math.pow(y, 2))+(6*x*y)+(2*x)+(2*y);
    }

    public static void main(String[] args) {
        //System.out.println("");
        Scanner scanner = new Scanner(System.in);

        Props x = new Props();
        System.out.println("H:" + x.getH());
        System.out.println("B:" + x.getB());

        Grid grid = new Grid(x);

        grid.generateGrid();


        double result=0.0;

        MPCTable mpcTable=new MPCTable();
        double [][] integralPoints = mpcTable.getMpc2();

        System.out.println("Integral 2D with 2 points:");
        for (int i = 0; i < 2; i++){
            for (int j = 0; j < 2; j++){
                result+= f(integralPoints[i][0], integralPoints[j][0]) * integralPoints[i][1] * integralPoints[j][1];
            }
        }
        System.out.println("Sum: "+result);


        result=0;
        integralPoints = mpcTable.getMpc3();
        System.out.println("Integral 2D with 3 points:");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                result+= f(integralPoints[i][0], integralPoints[j][0]) * integralPoints[i][1] * integralPoints[j][1];
            }
        }
        System.out.println("Sum: " + result);

    }

}
